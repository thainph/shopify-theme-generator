<?php

namespace Thainph\ShopifyThemeGenerator\Console;

use Illuminate\Console\Command;

class GenerateShopifyConfigCommand extends Command
{
    protected $signature = 'shopify-theme:config';

    protected $description = 'Config theme access';

    public function handle(): void
    {
        $this->generateConfigFile();
    }
    private function generateConfigFile()
    {
        $themeId = config('shopify-theme.theme.id');
        $themePassword = config('shopify-theme.theme.password');
        $themeStore = config('shopify-theme.theme.store');
        $themeEnv = config('app.env') === 'local' ? 'development' : 'production';
        $configYml = <<<YML
$themeEnv:
  password: $themePassword
  theme_id: "$themeId"
  store: $themeStore
YML;

        file_put_contents(config('shopify-theme.folder.source') . '/config.yml', $configYml);
        file_put_contents(config('shopify-theme.folder.dist') . '/config.yml', $configYml);
        $this->info('Access config generated!');
    }
}

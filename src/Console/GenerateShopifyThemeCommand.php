<?php

namespace Thainph\ShopifyThemeGenerator\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class GenerateShopifyThemeCommand extends Command
{
    protected $signature = 'shopify-theme:generate {--file=}';

    protected $description = 'Generate Shopify theme';

    public function handle(): void
    {
        $this->bindingEnvConfig();

        foreach (config('shopify-theme.binding') as $binding) {
            if (empty($this->option('file')) || $binding['to'] === $this->option('file')) {
                $this->bindingLiquidEmbed($binding);
            }
        }

    }

    private function bindingEnvConfig()
    {
        $configPath = config('shopify-theme.folder.source') . '/config';
        $distPath = config('shopify-theme.folder.dist') . '/config';

        foreach (config('shopify-theme.environment') as $env) {
            if (config('app.env') === $env) {
                if (file_exists($configPath . '/' . $env)) {
                    $copyPath = $configPath . '/' . $env. '/*';
                    shell_exec("rm -rf $distPath/*");
                    shell_exec("cp -R $copyPath $distPath");
                    $this->info('Environment '.$env.' configured!');
                } else {
                    $copyPath = $configPath . '/*';
                    shell_exec("rm -rf $distPath/*");
                    shell_exec("cp -R $copyPath $distPath");
                    $this->info('Environment '.$env.' config not found, all file copied!');
                }
            }
        }
    }
    private function bindingLiquidEmbed($binding)
    {
        $liquid = file_get_contents($binding['to']);
        $embedFiles = File::allFiles($binding['from']);
        $viewPath = Str::remove(resource_path('views'), $binding['from']);
        $viewPath = Str::replace('/', '.', $viewPath);

        foreach ($embedFiles as $file) {
            $sectionName = Str::remove('.blade.php', $file->getFilename());
            $sectionData = view($viewPath. '.' . $sectionName);
            $liquid = Str::replace('@' . $sectionName, $sectionData, $liquid);
        }

        $distFile = Str::replace(config('shopify-theme.folder.source'), config('shopify-theme.folder.dist'), $binding['to']);
        file_put_contents($distFile, $liquid);
        $this->info('Data embed to ' . $distFile);
    }
}

<?php
return [
    'environment' => [
        'local',
        'staging',
        'beta',
        'production',
    ],
    'folder' => [
        'source' => resource_path('views/liquid'),
        'dist' => resource_path('shopify_theme'),
        'embed' => resource_path('views/liquid_embed')
    ],
    'binding' => [
        [
            'from' => resource_path('views/liquid_embed/layout/theme'),
            'to' => resource_path('views/liquid/layout/theme.liquid'),
        ]
    ],
    'theme' => [
        'id' => env('SHOPIFY_THEME_ID'),
        'password' => env('SHOPIFY_THEME_PASSWORD'),
        'store' => env('SHOPIFY_THEME_STORE'),
    ]
];

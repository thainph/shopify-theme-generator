<?php
namespace Thainph\ShopifyThemeGenerator;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Http\Kernel;
use Thainph\ShopifyThemeGenerator\Console\GenerateShopifyConfigCommand;
use Thainph\ShopifyThemeGenerator\Console\GenerateShopifyThemeCommand;

class ShopifyThemeGeneratorServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/shopify-theme.php' => config_path('shopify-theme.php'),
        ], 'shopify-theme-config');


        if ($this->app->runningInConsole()) {
            $this->commands([
                GenerateShopifyConfigCommand::class,
                GenerateShopifyThemeCommand::class,
            ]);
        }

    }
}

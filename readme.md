# Shopify Theme Generator

Watch & bind data from laravel blade view to shopify theme

# Installation

1. Config in app.php ` Thainph\ShopifyThemeGenerator\ShopifyThemeGeneratorServiceProvider::class,`
2. Publish config file: `php artisan vendor:publish --tag=shopify-theme-config`
# How to use
Generate shopify theme config file
```
php artisan shopify-theme:config
```
Generate shopify theme
```
php artisan shopify-theme:generate {--file}

```
